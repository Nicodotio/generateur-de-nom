import unittest

from generateur_de_nom import (correspondanceNumeroUniversNom,
                               correspondanceNumeroUniversPrenom,
                               correspondanceLettreNom,
                               correspondanceLettrePrenom)
from config import dic_univers


class TestGenerateurDeNom(unittest.TestCase):
    def test_correspondance_numero_univers_nom(self):
        self.assertEqual(correspondanceNumeroUniversNom(
            dic_univers, "Star Wars"), 3)
        self.assertEqual(correspondanceNumeroUniversNom(
            dic_univers, "Game Of Thrones"), 1)
        self.assertEqual(correspondanceNumeroUniversNom(
            dic_univers, "Super Heros"), -1)

    def test_correspondance_numero_univers_prenom(self):
        self.assertEqual(correspondanceNumeroUniversPrenom(
            dic_univers, "Star Wars"), 1)
        self.assertEqual(correspondanceNumeroUniversPrenom(
            dic_univers, "Game Of Thrones"), 1)
        self.assertEqual(correspondanceNumeroUniversPrenom(
            dic_univers, "Super Heros"), 1)

    def test_correspondance_lettre_nom(self):
        self.assertEqual(correspondanceLettreNom(
            dic_univers, "Star Wars", "n"), "Grievous")
        self.assertEqual(correspondanceLettreNom(
            dic_univers, "Game Of Thrones", "a"), "Heartstriker")
        self.assertEqual(correspondanceLettreNom(
            dic_univers, "Super Heros", "z"), "Lightening")

    def test_correspondance_lettre_prenom(self):
        self.assertEqual(correspondanceLettrePrenom(
            dic_univers, "Star Wars", "e"), "Emperor")
        self.assertEqual(correspondanceLettrePrenom(
            dic_univers, "Game Of Thrones", "j"), "Bitter")
        self.assertEqual(correspondanceLettrePrenom(
            dic_univers, "Super Heros", "l"), "Invisible")
