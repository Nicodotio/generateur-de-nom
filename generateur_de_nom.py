"""
    The ``generateur_de_nom`` module
    ================================

    Un script qui permet de générer des noms de différents univers.
"""


from tkinter import Tk, Text, StringVar, PhotoImage
from tkinter.ttk import Button, Combobox, Label, Entry

from config import dic_univers, list_univers


def correspondanceNumeroUniversNom(dic_config, univers):
    """
    Fournit la position de la lettre qui sera utilisée dans le nom.

    :param dic_config: dictionnaire de configuration contenant les données des univers
    :type dic_config: dict
    :param univers: univers choisi
    :type univers: str
    :return: position de la lettre du nom
    :rtype: int
    """
    return dic_config.get(univers).get("num_lettre_nom")


def correspondanceNumeroUniversPrenom(dic_config, univers):
    """
    Fournit la position de la lettre qui sera utilisée dans le prénom.

    :param dic_config: dictionnaire de configuration contenant les données des univers
    :type dic_config: dict
    :param univers: univers choisi
    :type univers: str
    :return: position de la lettre du prénom
    :rtype: int
    """
    return dic_config.get(univers).get("num_lettre_prenom")


def correspondanceLettreNom(dic_config, univers, lettre):
    """
    Associe une lettre à un nom de l'univers.

    :param dic_config: dictionnaire de configuration contenant les données des univers
    :type dic_config: dict
    :param univers: univers choisi
    :type univers: str
    :return: nom venant de l'univers
    :rtype: str
    """
    return dic_config.get(univers).get("correspondance_lettre_nom").get(lettre)


def correspondanceLettrePrenom(dic_config, univers, lettre):
    """
    Associe une lettre à un prénom de l'univers.

    :param dic_config: dictionnaire de configuration contenant les données des univers
    :type dic_config: dict
    :param univers: univers choisi
    :type univers: str
    :return: prénom venant de l'univers
    :rtype: str
    """
    return dic_config.get(univers).get("correspondance_lettre_prenom").get(lettre)


def clicBoutonGenerer():
    """
    Vérifie la validité de la saisie, puis génère le nom à l'aide des
    fonctions précédentes.
    """
    nom = entree_nom.get()
    prenom = entree_prenom.get()
    univers = combobox_univers.get()

    num_lettre_nom = correspondanceNumeroUniversNom(dic_univers, univers)
    num_lettre_prenom = correspondanceNumeroUniversPrenom(
        dic_univers, univers)

    # Il est nécessaire d'utiliser la valeur absolue de la longueur, car
    # il est posssible d'avoir -1 par exemple pour signifier la dernière lettre.
    if((len(nom) < abs(num_lettre_nom)) | (len(prenom) < abs(num_lettre_prenom))):
        label_resultat.config(text="Saisie invalide !")
    else:
        nom = nom.lower()
        prenom = prenom.lower()

        if(num_lettre_nom > 0):
            lettre_nom = nom[num_lettre_nom-1]
        else:
            lettre_nom = nom[num_lettre_nom]

        # Il faut décaler de 1 sauf si on part de la fin avec -1 par exemple.
        if(num_lettre_prenom > 0):
            lettre_prenom = prenom[num_lettre_prenom-1]
        else:
            lettre_prenom = prenom[num_lettre_prenom]

        if((lettre_nom < "a") | (lettre_nom > "z") | (lettre_prenom < "a") | (lettre_prenom > "z")):
            label_resultat.config(text="Saisie invalide !")
        else:
            new_nom = correspondanceLettreNom(dic_univers, univers, lettre_nom)
            new_prenom = correspondanceLettrePrenom(
                dic_univers, univers, lettre_prenom)

            label_resultat.config(text="".join(
                ["Votre nouveau nom est ", new_prenom, " ", new_nom, "."]))


if __name__ == "__main__":
    frame = Tk()
    frame.title("Générateur de nom")
    icon = PhotoImage(file="icon.gif")
    frame.iconphoto(False, icon)
    frame.minsize(240, 150)

    frame.rowconfigure(0, weight=1)
    frame.rowconfigure(1, weight=1)
    frame.rowconfigure(2, weight=1)
    frame.rowconfigure(3, weight=1)
    frame.rowconfigure(4, weight=1)

    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)

    label_prenom = Label(frame, text="Prénom :")
    label_prenom.grid(column=0, row=0, sticky="e", padx=10, pady=5)

    entree_prenom = Entry(frame)
    entree_prenom.grid(column=1, row=0, sticky="ew", padx=10, pady=5)

    label_nom = Label(frame, text="Nom :")
    label_nom.grid(column=0, row=1, sticky="e", padx=10, pady=5)

    entree_nom = Entry(frame)
    entree_nom.grid(column=1, row=1, sticky="ew", padx=10, pady=5)

    label_univers = Label(frame, text="Univers :")
    label_univers.grid(column=0, row=2, sticky="e", padx=10, pady=5)

    combobox_univers = Combobox(
        frame, values=list_univers, state="readonly")
    combobox_univers.grid(column=1, row=2, sticky="ew", padx=10, pady=5)
    combobox_univers.current(0)

    bouton_generer = Button(frame, text="Générer le nom",
                            command=clicBoutonGenerer)
    bouton_generer.grid(column=1, row=3, sticky="ew", padx=10, pady=5)

    label_resultat = Label(frame)
    label_resultat.grid(column=0, row=4, columnspan=2)

    frame.mainloop()
